package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class DemoTestingSite {
    @FindBy(xpath = "(//a[@href=\"https://www.globalsqa.com/testers-hub/\"])[1]")
    public WebElement testerHub;
    @FindBy(xpath = "(//a[@href=\"https://www.globalsqa.com/demo-site/\"])[1]")
    public WebElement testingSite;
    @FindBy(xpath = "(//div[@class=\"price_column \"])[1]//ul//li[@class=\"price_footer\"]")
    public List<WebElement> firstStepColumn;
    @FindBy(xpath = "(//div[@class=\"price_column \"])[2]//ul//li[@class=\"price_footer\"]")
    public List<WebElement> secondStepColumn;
    @FindBy(xpath = "(//div[@class=\"price_column \"])[3]//ul//li[@class=\"price_footer\"]")
    public List<WebElement> thirdStepColumn;

    WebDriver driver;
    WebDriverWait wait;
    Actions actions;

    public DemoTestingSite(WebDriver driver) {
        wait = new WebDriverWait(driver, 60);
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void demoTestingSite() {
        actions = new Actions(driver);
        actions.moveToElement(testerHub).perform();
        testingSite.click();
    }

}
