package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class ProgressBar {
    @FindBy(xpath = "(//a[@href=\"https://www.globalsqa.com/testers-hub/\"])[1]")
    public WebElement testerHub;
    @FindBy(xpath = "(//a[@href=\"https://www.globalsqa.com/demo-site/\"])[1]")
    public WebElement testingSite;
    @FindBy(xpath = "(//span[text()=\"Progress Bar\"])[1]")
    public WebElement progressBar;
    @FindBy(xpath = "//iframe[@class=\"demo-frame lazyloaded\"]")
    public WebElement iframe;
    @FindBy(id = "downloadButton")
    public WebElement downloadButton;
    @FindBy(xpath = "//div[text()=\"Complete!\"]")
    public WebElement completedStatus;

    WebDriver driver;
    WebDriverWait wait;
    Actions actions;

    public ProgressBar(WebDriver driver) {
        wait = new WebDriverWait(driver, 60);
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void progressBar() {
        actions = new Actions(driver);
        actions.moveToElement(testerHub).moveToElement(testingSite).perform();
        progressBar.click();
    }

    public void progressBarCheckerMethod() {
        driver.switchTo().frame(iframe);
        downloadButton.click();
        wait.until(ExpectedConditions.visibilityOf(completedStatus));
    }

    public boolean verificationOfStatus() {
        return completedStatus.isDisplayed();
    }

}
