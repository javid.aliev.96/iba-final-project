package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.SimpleDateFormat;
import java.util.NoSuchElementException;

public class DatePicker {
    @FindBy(xpath = "(//a[@href=\"https://www.globalsqa.com/testers-hub/\"])[1]")
    public WebElement testerHub;
    @FindBy(xpath = "(//a[@href=\"https://www.globalsqa.com/demo-site/\"])[1]")
    public WebElement testingSite;
    @FindBy(xpath = "//span[text()=\"DatePicker\"]")
    public WebElement datePicker;
    @FindBy(xpath = "//iframe[@class=\"demo-frame lazyloaded\"]")
    public WebElement iframe;
    @FindBy(xpath = "//input[@id=\"datepicker\"]")
    public WebElement calendarBox;
    @FindBy(xpath = "//td[@class=\" ui-datepicker-days-cell-over  ui-datepicker-today\"]")
    public WebElement currentDay;
    @FindBy(xpath = "//td[@class=\" ui-datepicker-week-end ui-datepicker-days-cell-over  ui-datepicker-today\"]")
    public WebElement currentDayWeekend;

    WebDriver driver;
    WebDriverWait wait;
    Actions actions;

    public void datePicker() {
        actions = new Actions(driver);
        actions.moveToElement(testerHub).moveToElement(testingSite).perform();
        actions.moveToElement(datePicker).perform();
        datePicker.click();
    }

    public DatePicker(WebDriver driver) {
        wait = new WebDriverWait(driver, 60);
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void datePickerMethod() {
        driver.switchTo().frame(iframe);
        calendarBox.click();
        try {
            currentDay.click();
        } catch (NoSuchElementException noSuchElementException) {
            currentDayWeekend.click();
        }
    }

    public String selectedViaSeleniumDate() {
        return calendarBox.getAttribute("value");
    }

    public String actualSystemDate() {
        SimpleDateFormat actualSystemDate = new SimpleDateFormat("MM/dd/yyyy");
        return actualSystemDate.format(System.currentTimeMillis());
    }
}

