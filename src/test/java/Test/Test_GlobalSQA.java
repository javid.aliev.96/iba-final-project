package Test;

import Pages.DatePicker;
import Pages.DemoTestingSite;
import Pages.ProgressBar;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import static org.testng.Assert.*;

//TODO PLEASE NOTE THAT CHROMEDRIVER VERSION USED DURING DEVELOPMENT AND UPLOAD IS: 91.0.4472.114

public class Test_GlobalSQA {

    private WebDriver driver;
    DemoTestingSite objDemoTestingSite;
    DatePicker objDatePicker;
    ProgressBar objProgressBar;

    @BeforeMethod()
    void setInfo() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.get("https://www.globalsqa.com/");
    }

    @AfterMethod
    void endTesting() {
        driver.quit();
    }

    @Test(priority = 1)
    void columnSizeVerifying() {
        final int acceptedLengthForFirstTask = 6;
        objDemoTestingSite = new DemoTestingSite(driver);
        objDemoTestingSite.demoTestingSite();
        assertEquals(objDemoTestingSite.firstStepColumn.size(), acceptedLengthForFirstTask);
        assertEquals(objDemoTestingSite.secondStepColumn.size(), acceptedLengthForFirstTask);
        assertEquals(objDemoTestingSite.thirdStepColumn.size(), acceptedLengthForFirstTask);
        System.out.println("Number of elements in first column is: " + objDemoTestingSite.firstStepColumn.size());
        System.out.println("Number of elements in second column is: " + objDemoTestingSite.secondStepColumn.size());
        System.out.println("Number of elements in third column is: " + objDemoTestingSite.thirdStepColumn.size());
    }

    @Test(priority = 2)
    void dateFormatVerifying() {
        objDatePicker = new DatePicker(driver);
        objDatePicker.datePicker();
        objDatePicker.datePickerMethod();
        assertEquals(objDatePicker.selectedViaSeleniumDate(), objDatePicker.actualSystemDate());
        System.out.println("Selected Date via Autotest is: " + objDatePicker.selectedViaSeleniumDate());
        System.out.println("Actual Date in System is: " + objDatePicker.actualSystemDate());
    }

    @Test(priority = 3)
    void progressBarVerifying() {
        objProgressBar = new ProgressBar(driver);
        objProgressBar.progressBar();
        objProgressBar.progressBarCheckerMethod();
        assertTrue(objProgressBar.verificationOfStatus());
        System.out.println("Status of download completion is: " + objProgressBar.verificationOfStatus());
    }
}
